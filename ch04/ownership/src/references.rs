pub fn references() {
    // passing the reference
    let s1 = String::from("hello");
    let len = calculate_length(&s1); // this is reference borrowing
    println!("The length of '{s1}' is {len}.");

    let mut s = String::from("hello");

    change(&mut s);
    println!("{s}");
    println!();

    // can have only one mutable borrowing
    // second borrowing will produce compiler error
    // let r1 = &mut s;
    // let r2 = &mut s;
    // println!("{r1}, {r2}");

    {
        let r1 = &mut s;
        println!("{r1}");
    } // r1 goes out of scope here, so we can make a new reference with no problems
    let r2 = &mut s;
    println!("{r2}");
    println!();

    // cannot combine immutable and mutable borrowing
    // let r1 = &s; // no problem
    // let r2 = &s; // no problem
    // let r3 = &mut s; // BIG PROBLEM
    // println!("{r1}, {r2}, and {r3}");

    let r1 = &s; // no problem
    let r2 = &s; // no problem
    println!("{r1} and {r2}"); // Variables r1 and r2 will not be used after this point.

    let r3 = &mut s; // no problem
    println!("{r3}");
    println!();

    // Slices
    slices();
}

fn calculate_length(s: &String) -> usize { // s is a reference to a String
    s.len()
} // Here, s goes out of scope. But because it does not have ownership of what
// it refers to, the String is not dropped.

// next function will not compile, because it tries to modify immutable variable
// fn change(some_string: &String) {
//     some_string.push_str(", world");
// }

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}

// dangling references
// this function will not compile
// fn dangle() -> &String { // dangle returns a reference to a String
//
//     let s = String::from("hello"); // s is a new String
//
//     &s // we return a reference to the String, s
// } // Here, s goes out of scope and is dropped, so its memory goes away. Danger!

fn slices() {
    let mut s = String::from("hello world");

    let word = first_word(&s); // word will get the value 5
    println!("{}", word);

    s.clear(); // this empties the String, making it equal to ""

    // word still has the value 5 here, but there's no more string that
    // we could meaningfully use the value 5 with. word is now totally invalid!

    // string slices
    let s = String::from("hello world");

    let hello = &s[0..5];
    let world = &s[6..11];
    println!("{}", hello);
    println!("{}", world);
    println!();

    // following 2 lines are equal
    let slice = &s[0..2];
    println!("{}", slice);
    let slice = &s[..2];
    println!("{}", slice);

    let len = s.len();
    // same 2 following lines are equal
    let slice = &s[3..len];
    println!("{}", slice);
    let slice = &s[3..];
    println!("{}", slice);
    println!();

    let s = String::from("hello world");
    let word = first_word_with_slice(&s);
    // s.clear(); // error!
    println!("the first word is: {word}");
    println!();

    let my_string = String::from("hello world");

    // `first_word` works on slices of `String`s, whether partial or whole.
    let word = first_word_improved(&my_string[0..6]);
    println!("the first word is: {word}");
    let word = first_word_improved(&my_string[..]);
    println!("the first word is: {word}");
    // `first_word_improved` also works on references to `String`s, which
    // are equivalent to whole slices of `String`s.
    let word = first_word_improved(&my_string);
    println!("the first word is: {word}");

    let my_string_literal = "hello world";

    // `first_word_improved` works on slices of string literals, whether partial or whole.
    let word = first_word_improved(&my_string_literal[0..6]);
    println!("the first word is: {word}");
    let word = first_word_improved(&my_string_literal[..]);
    println!("the first word is: {word}");

    // Because string literals *are* string slices already, this works too, without the slice syntax!
    let word = first_word_improved(my_string_literal);
    println!("the first word is: {word}");

    // array slices
    let a = [1, 2, 3, 4, 5];
    let slice = &a[1..3];
    assert_eq!(slice, &[2, 3]);
}

fn first_word(s: &String) -> usize {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return i;
        }
    }
    s.len()
}

fn first_word_with_slice(s: &String) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }
    &s[..]
}

fn first_word_improved(s: &str) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }
    &s[..]
}
