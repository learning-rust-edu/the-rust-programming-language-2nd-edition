mod ownership;
mod references;

fn main() {
    // Ownership
    ownership::ownership();
    println!();
    // References and Borrowing
    references::references();
    println!();
}
