use std::fs::{self, File};
use std::io::{self, ErrorKind, Read};

fn remove_file(file: &str) {
    fs::remove_file(file).expect("no such file");
}

fn simple_example() {
    let file_name = "hello.txt";
    let greeting_file_result = File::open(&file_name);

    let greeting_file = match greeting_file_result {
        Ok(file) => file,
        Err(error) => {
            panic!("Problem opening the file: {:?}", error);
        },
    };
    remove_file(&file_name);
}

fn matching_on_different_errors(file_name: &str) {
    let greeting_file_result = File::open(&file_name);

    let greeting_file = match greeting_file_result {
        Ok(file) => file,
        Err(error) => match error.kind() {
            ErrorKind::NotFound => {
                match File::create(&file_name) {
                    Ok(fc) => fc,
                    Err(e) => panic!("Problem creating the file: {:?}", e),
                }
            },
            other_error => {
                panic!("Problem opening the file: {:?}", other_error);
            }
        }
    };
    remove_file(&file_name);
}

fn alt_match(file_name: &str) {
    let greeting_file = File::open(&file_name).unwrap_or_else(|error| {
        if error.kind() == ErrorKind::NotFound {
            File::create(&file_name).unwrap_or_else(|err| {
                panic!("Problem creating the file: {:?}", err);
            })
        } else {
            panic!("Problem opening the file: {:?}", error);
        }
    });
    remove_file(&file_name);
}

fn shortcuts(file_name: &str) {
    let greeting_file = File::open("hello.txt").unwrap();
}

fn shortcuts_with_expect(file_name: &str) {
    let greeting_file = File::open("hello.txt")
        .expect("hello.txt should be included in this project");
}

fn read_username_from_file(file_name: &str) -> Result<String, io::Error> {
    let username_file_result = File::open(&file_name);

    let mut username_file = match username_file_result {
        Ok(file) => file,
        Err(e) => return Err(e),
    };

    let mut username = String::new();

    match username_file.read_to_string(&mut username) {
        Ok(_) => Ok(username),
        Err(e) => Err(e),
    }
}

fn read_username_from_file_shortcuts(file_name: &str) -> Result<String, io::Error> {
    let mut username_file = File::open(&file_name)?;
    let mut username = String::new();
    username_file.read_to_string(&mut username)?;
    Ok(username)
}

fn read_username_from_file_shorter(file_name: &str) -> Result<String, io::Error> {
    let mut username = String::new();
    File::open(&file_name)?.read_to_string(&mut username)?;
    Ok(username)
}

fn read_username_from_file_shortest(file_name: &str) -> Result<String, io::Error> {
    fs::read_to_string(&file_name)
}

fn last_char_of_first_line(text: &str) -> Option<char> {
    text.lines().next()?.chars().last()
}

fn main() {
    let file_name = "hello.txt";
    // Recoverable Errors with Result
//    simple_example();

    // Matching on Different Errors
    matching_on_different_errors(&file_name);

    // Alternatives to Using match with Result<T, E>
    alt_match(&file_name);

    // Shortcuts for Panic on Error: unwrap and expect
//    shortcuts(&file_name);
//    shortcuts_with_expect(&file_name);

    // Propagating Errors
    match read_username_from_file(&file_name) {
        Ok(username) => {
            println!("Username: {}", username);
        },
        Err(error) => {
            println!("Error caught during username reading: {:?}", error);
        }
    };

    // A Shortcut for Propagating Errors: The ? Operator
    match read_username_from_file_shortcuts(&file_name) {
        Ok(username) => {
            println!("Username: {}", username);
        },
        Err(error) => {
            println!("Error caught during username reading: {:?}", error);
        }
    };
    match read_username_from_file_shorter(&file_name) {
        Ok(username) => {
            println!("Username: {}", username);
        },
        Err(error) => {
            println!("Error caught during username reading: {:?}", error);
        }
    };
    match read_username_from_file_shortest(&file_name) {
        Ok(username) => {
            println!("Username: {}", username);
        },
        Err(error) => {
            println!("Error caught during username reading: {:?}", error);
        }
    };
    println!();

    // using ? with Option<T>
    println!("{:?}", last_char_of_first_line(""));
    println!("{:?}", last_char_of_first_line("\nHello"));
    println!("{:?}", last_char_of_first_line("Hello"));
    println!("{:?}", last_char_of_first_line("Goodbye\nHello"));
}
