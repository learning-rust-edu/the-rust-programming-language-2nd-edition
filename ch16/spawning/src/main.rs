use std::thread;
use std::time::Duration;

fn main() {
    thread::spawn(|| {
        for i in 1..10 {
            println!("hi number {i} from the first spawned thread!");
            thread::sleep(Duration::from_millis(2));
        }
    });

    let handle = thread::spawn(|| {
        for i in 1..10 {
            println!("hi number {i} from the second spawned thread!");
            thread::sleep(Duration::from_millis(1));
        }
    });

    for i in 1..5 {
        println!("hi number {i} from the main thread!");
        thread::sleep(Duration::from_millis(1));
    }

    handle.join().unwrap();
    for i in 1..5 {
        println!("hi number {i} from the main thread second time!");
        thread::sleep(Duration::from_millis(1));
    }
}
