//use std::rc::Rc;
use std::sync::{Arc, Mutex};
use std::thread;

fn main() {
    // Using Mutexes to Allow Access to Data from One Thread at a Time
    use_mutex();
    println!();

    // Sharing a Mutex<T> Between Multiple Threads
    multi_thread_mutex();
}

fn multi_thread_mutex() {
    // Rc is not thread-safe, so it cannot be used with Mutex
//    let counter = Rc::new(Mutex::new(0));
    // Atomic Reference Counting with Arc<T>
    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    for _ in 0..10 {
//        let counter = Rc::clone(&counter);
        let counter = Arc::clone(&counter);
        let handle = thread::spawn(move || {
            let mut num = counter.lock().unwrap();
            *num += 1;
        });
        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }

    println!("Result: {}", *counter.lock().unwrap());
}

fn use_mutex() {
    let m = Mutex::new(5);

    {
        let mut num = m.lock().unwrap();
        *num = 6;
    } // the lock is released automatically at the end of the scope

    println!("m = {:?}", m);
}
