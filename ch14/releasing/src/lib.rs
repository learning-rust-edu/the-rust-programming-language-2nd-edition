//! # My Crate
//!
//! `releasing` is a collection of utilities to make performing
//! certain calculations more convenient.

/// Adds one to the number given.
///
/// To see this documentation execute following command:
/// `cargo doc --open`
///
/// # Examples
///
/// ```
/// let arg = 5;
/// let answer = releasing::add_one(arg);
///
/// assert_eq!(6, answer);
/// ```
pub fn add_one(x: i32) -> i32 {
    x + 1
}


// to make enums and mix function to be visible on crate's root level
pub use self::kinds::PrimaryColor;
pub use self::kinds::SecondaryColor;
pub use self::utils::mix;

pub mod kinds {
    /// The primary colors according to the RYB color model.
    pub enum PrimaryColor {
        Red,
        Yellow,
        Blue,
    }

    /// The secondary colors according to the RYB color model.
    pub enum SecondaryColor {
        Orange,
        Green,
        Purple,
    }
}

pub mod utils {
    use crate::kinds::*;

    /// Combines two primary colors in equal amounts to create
    /// a secondary color.
    pub fn mix(
        c1: PrimaryColor,
        c2: PrimaryColor,
    ) -> SecondaryColor {
        match c1 {
            PrimaryColor::Red if c2 == PrimaryColor::Yellow => SecondaryColor::Orange,
            PrimaryColor::Red if c2 == PrimaryColor::Blue => SecondaryColor::Purple,
            PrimaryColor::Yellow if c2 == PrimaryColor::Red => SecondaryColor::Orange,
            PrimaryColor::Yellow if c2 == PrimaryColor::Blue => SecondaryColor::Green,
            PrimaryColor::Blue if c2 == PrimaryColor::Yellow => SecondaryColor::Green,
            PrimaryColor::Blue if c2 == PrimaryColor::Red => SecondaryColor::Purple,
            _ => c1, // both color are same
        }
    }
}
