pub fn add_one(x: i32) -> i32 {
    x + 1
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn add_to_positive() {
        let result = add_one(2);
        assert_eq!(result, 3);
    }

    #[test]
    fn add_to_negative() {
        let result = add_one(-2);
        assert_eq!(result, -1);
    }
}
