fn main() {
    // Using the Newtype Pattern for Type Safety and Abstraction
    // Creating Type Synonyms with Type Aliases
    type_aliases();

    // Dynamically Sized Types and the Sized Trait
    dynamic_sized();
}

fn dynamic_sized() {
    // it’s not possible to create a variable holding a dynamically sized type.
    // let s1: str = "Hello there!";
    // let s2: str = "How's it going?";
    let s1: &str = "Hello there!";
    let s2: &str = "How's it going?";
}

// The Never Type That Never Returns
// fn bar() -> ! {
//     --snip--
// }

fn type_aliases() {
    type Kilometers = i32;

    let x: i32 = 5;
    let y: Kilometers = 5;

    println!("x + y = {}", x + y);
}
