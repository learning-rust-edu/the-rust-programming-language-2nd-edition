# The Rust Programming Language 2nd Edition

Sample code for _"The Rust Programming Language, 2nd Edition"_ book from __O'Reilly__ learning platform published by __No Starch Press__.

## License
This project is licensed under the [MIT License](LICENSE).
