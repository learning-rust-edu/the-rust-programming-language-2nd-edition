fn main() {
    let user1 = User {
        active: true,
        username: String::from("someusername123"),
        email: String::from("someone@example.com"),
        sign_in_count: 1,
    };
    println!("user is active {}", user1.active);

    let mut user1 = User {
        active: true,
        username: String::from("someusername123"),
        email: String::from("someone@example.com"),
        sign_in_count: 1,
    };
    user1.email = String::from("anotheremail@example.com");
    println!("user's email: {}", user1.email);

    let user1 = build_user(
        String::from("anotheremail@example.com"),
        String::from("someusername123")
    );
    println!("username: {}", user1.username);

    let user2 = User {
        active: user1.active,
        username: user1.username,
        email: String::from("another@example.com"),
        sign_in_count: user1.sign_in_count,
    };
    println!("user's email: {}", user2.email);

    // can no longer use user1 after creating user2 because the String in the username field of
    // user1 was moved into user2
    let user1 = build_user(
        String::from("anotheremail@example.com"),
        String::from("someusername123")
    );

    // same as above with less code
    let user2 = User {
        email: String::from("another@example.com"),
        ..user1
    };
    println!("user's email: {}", user2.email);
    println!();

    // Tuple Struct without named fields
    tuple_structs();
    println!();

    // Unit-Like Structs Without Any Fields
    unit_like_structs();
    println!();
}

struct User {
    active: bool,
    username: String,
    email: String,
    sign_in_count: u64,
}

fn build_user(email: String, username: String) -> User {
    User {
        active: true,
        username,
        email,
        sign_in_count: 1,
    }
}

#[derive(Debug)]
struct Color(i32, i32, i32);
#[derive(Debug)]
struct Point(i32, i32, i32);

fn tuple_structs() {
    let black = Color(0, 0, 0);
    let origin = Point(0, 0, 0);
    println!("{:?})", black);
    println!("{:?}", origin);
}

#[derive(Debug)]
struct AlwaysEqual;

fn unit_like_structs() {
    let subject = AlwaysEqual;
    println!("{:?}", subject);
}
