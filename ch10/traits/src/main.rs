use traits::{NewsArticle, notify, returns_summarizable, Summary, Summary2, Summary3, Tweet};

fn main() {
    // Traits: Defining Shared Behavior
    create_and_implement_trait();
    println!();
    // Traits as Parameters
    traits_as_parameters();
    println!();
    // Returning Types That Implement Traits
    return_trait();
}

fn return_trait() {
    let tweet = returns_summarizable();
    println!("1 new tweet: {}", tweet.summarize());
}

fn traits_as_parameters() {
    let tweet = Tweet {
        username: String::from("horse_ebooks"),
        content: String::from(
            "of course, as you probably already know, people",
        ),
        reply: false,
        retweet: false,
    };
    notify(&tweet);
}

fn create_and_implement_trait() {
    let tweet = Tweet {
        username: String::from("horse_ebooks"),
        content: String::from(
            "of course, as you probably already know, people",
        ),
        reply: false,
        retweet: false,
    };
    println!("1 new tweet: {}", tweet.summarize());
    println!();

    // default implementation
    let article = NewsArticle {
        headline: String::from(
            "Penguins win the Stanley Cup Championship!"
        ),
        location: String::from("Pittsburgh, PA, USA"),
        author: String::from("Iceburgh"),
        content: String::from(
            "The Pittsburgh Penguins once again are the best \
         hockey team in the NHL.",
        ),
    };
    println!("New article available! {}", article.summarise());

    let tweet = Tweet {
        username: String::from("horse_ebooks"),
        content: String::from(
            "of course, as you probably already know, people",
        ),
        reply: false,
        retweet: false,
    };
    println!("1 new tweet: {}", tweet.summarise());
}
