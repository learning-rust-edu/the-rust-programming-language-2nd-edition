use std::fmt::{Debug, Display};

// Defining a Trait
pub trait Summary {
    fn summarize(&self) -> String;
}

pub trait Summary2 {
    fn summarise(&self) -> String { // default implementation
        String::from("(Read more...)")
    }
}

pub trait Summary3 {
    fn summarise_author(&self) -> String;

    fn summarise(&self) -> String {
        format!(
            "(Read more from {}...)",
            self.summarise_author(),
        )
    }
}

// Implementing a Trait on a Type
pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

impl Summary for NewsArticle {
    fn summarize(&self) -> String {
        format!(
            "{}, by {} ({})",
            self.headline,
            self.author,
            self.location,
        )
    }
}

impl Summary2 for NewsArticle {} // empty impl will use default trait implementation

pub struct Tweet {
    pub username: String,
    pub content: String,
    pub reply: bool,
    pub retweet: bool,
}

impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!("{}: {}", self.username, self.content)
    }
}

impl Summary3 for Tweet {
    fn summarise_author(&self) -> String {
        format!("@{}", self.username)
    }
}


// Traits as Parameters
pub fn notify(item: &impl Summary) {
    println!("Breaking news! {}", item.summarize());
}

// above is syntax sugar for a longer form of trait bound
pub fn notify2<T: Summary>(item: &T) {
    println!("Breaking news! {}", item.summarize());
}

// same but with 2 parameters: with impl, which allows multiple type of input
pub fn notify3(item1: &impl Summary, item2: &impl Summary) {
    println!("Breaking news! {}, {}", item1.summarize(), item2.summarize());
}

// the below is almost the same, except it allows only single type of input
pub fn notify4<T: Summary>(item1: &T, item2: &T) {
    println!("Breaking news! {}, {}", item1.summarize(), item2.summarize());
}

// Specifying Multiple Trait Bounds with the + Syntax
pub fn notify5(item: &(impl Summary + Display)) {
    println!("Breaking news! {}", item);
}

pub fn notify6<T: Summary + Display>(item: &T) {
    println!("Breaking news! {}", item);
}

// Clearer Trait Bounds with where Clauses
// instead of writing this:
fn some_function1<T: Display + Clone, U: Clone + Debug>(t: &T, u: &U) -> i32 {
    println!("{}, {:?}", t, u);
    5
}

// can use a where clause, like this:
fn some_function2<T, U>(t: &T, u: &U) -> i32
    where
        T: Display + Clone,
        U: Clone + Debug,
{
    println!("{}, {:?}", t, u);
    5
}

// Returning Types That Implement Traits
pub fn returns_summarizable() -> impl Summary {
    Tweet {
        username: String::from("horse_ebooks"),
        content: String::from(
            "of course, as you probably already know, people",
        ),
        reply: false,
        retweet: false,
    }
}

// Using Trait Bounds to Conditionally Implement Methods

struct Pair<T> {
    x: T,
    y: T,
}

impl<T> Pair<T> {
    fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}

impl<T: Display + PartialOrd> Pair<T> {
    fn cmp_display(&self) {
        if self.x >= self.y {
            println!("The largest member is x = {}", self.x);
        } else {
            println!("The largest member is y = {}", self.y);
        }
    }
}
