fn main() {
    // Removing Duplication by Extracting a Function
    let number_list = vec![34, 50, 25, 100, 65];
    let mut largest = &number_list[0];

    for number in &number_list {
        if number > largest {
            largest = number;
        }
    }
    println!("The largest number is {largest}");

    // to find largest number in the list, can duplicate the logic
    let number_list = vec![102, 34, 6000, 89, 54, 2, 43, 8];
    let mut largest = &number_list[0];

    for number in &number_list {
        if number > largest {
            largest = number;
        }
    }
    println!("The largest number is {largest}");
    println!();

    // better is to extract duplicated logic into separate function and use the function
    let number_list1 = vec![34, 50, 25, 100, 65];
    let number_list2 = vec![102, 34, 6000, 89, 54, 2, 43, 8];

    let result = largest_i32(&number_list1);
    println!("The largest number is {result}");

    let result = largest_i32(&number_list2);
    println!("The largest number is {result}");
    println!();

    // find largest element in list of characters
    let char_list = vec!['y', 'm', 'a', 'q'];
    let result = largest_char(&char_list);
    println!("The largest char is {result}");
    println!();

    // Generic Data Types
    generic_in_function();
    println!();

    // In Struct Definitions
    generic_in_struct();
    println!();

    // In Enum Definitions
    // good examples are Option<T> and Result<T, E>

    // In Method Definitions
    generic_in_method();
    println!();

    // Performance of Code Using Generics
    generic_performance();
}

fn generic_performance() {
    let integer = Some(5);
    let float = Some(5.0);
    println!("integer {:?}", integer);
    println!("float {:?}", float);

    // Compiler optimise (performs monomorphization) the code above into something like this
    let integer = Option_i32::Some(5);
    let float = Option_f64::Some(5.0);
    println!("integer {:?}", integer);
    println!("float {:?}", float);
}

#[derive(Debug)]
enum Option_i32 {
    Some(i32),
    None,
}

#[derive(Debug)]
enum Option_f64 {
    Some(f64),
    None,
}

fn generic_in_method() {
    let p = Point { x: 5, y: 10 };
    println!("p.x = {}", p.x());

    let p = Point { x: 5.0, y: 6.0 };
    println!("p.distance = {}", p.distance_from_origin());

    let p1 = Point2 { x: 5, y: 10.4 };
    let p2 = Point2 { x: "Hello", y: 'c' };

    let p3 = p1.mixup(p2);
    println!("p3.x = {}, p3.y = {}", p3.x, p3.y);
}

#[derive(Debug)]
struct Point<T> {
    x: T,
    y: T,
}

impl <T> Point<T> {
    fn x(&self) -> &T {
        &self.x
    }
}

impl Point<f32> {
    fn distance_from_origin(&self) -> f32 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }
}

#[derive(Debug)]
struct Point2<X1, Y1> {
    x: X1,
    y: Y1,
}

impl <X1, Y1> Point2<X1, Y1> {
    fn mixup<X2, Y2>(self, other: Point2<X2, Y2>) -> Point2<X1, Y2> {
        Point2 {
            x: self.x,
            y: other.y,
        }
    }
}

fn generic_in_struct() {
    let integer = Point { x: 5, y: 10 };
    println!("{:?}", integer);
    let float = Point { x: 1.0, y: 4.0 };
    println!("{:?}", float);
    // next line won't compile
//    let wont_work = Point { x: 5, y: 4.0 };
    println!();

    let both_integer = Point2 { x: 5, y: 10 };
    let both_float = Point2 { x: 1.0, y: 4.0 };
    let integer_and_float = Point2 { x: 5, y: 4.0 };
    println!("{:?}", both_integer);
    println!("{:?}", both_float);
    println!("{:?}", integer_and_float);
}

fn generic_in_function() {
    // In Function Definitions
    let number_list = vec![34, 50, 25, 100, 65];

    let result = largest(&number_list);
    println!("The largest number is {result}");

    let char_list = vec!['y', 'm', 'a', 'q'];

    let result = largest(&char_list);
    println!("The largest char is {result}");
}

fn largest<T: PartialOrd>(list: &[T]) -> &T {
    let mut largest = &list[0];

    for item in list {
        if item > largest {
            largest = item;
        }
    }
    largest
}

fn largest_i32(list: &[i32]) -> &i32 {
    let mut largest = &list[0];

    for number in list {
        if number > largest {
            largest = number;
        }
    }
    largest
}

fn largest_char(list: &[char]) -> &char {
    let mut largest = &list[0];

    for item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}
