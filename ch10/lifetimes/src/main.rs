use std::fmt::Display;

fn main() {
    // Preventing Dangling References with Lifetimes
    dangling_refs();
    no_dangling_ref();
    println!();

    // Generic Lifetimes in Functions
    function_lifetimes();
    println!();

    // Lifetime Annotations in Struct Definitions
    struct_lifetimes();

    // The Static Lifetime
    let s: &'static str = "I have a static lifetime."; // All string literals have the 'static lifetime
    println!("{}", s);
}

// Generic Type Parameters, Trait Bounds, and Lifetimes Together
fn longest_with_an_announcement<'a, T>(
    x: &'a str,
    y: &'a str,
    ann: T,
) -> &'a str
    where
        T: Display,
{
    println!("Announcement! {ann}");
    if x.len() > y.len() {
        x
    } else {
        y
    }
}

// Lifetime Elision
// function below compiles without lifetime annotations
fn first_word(s: &str) -> &str {
    let bytes = s.as_bytes();
    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }
    &s[..]
}

#[derive(Debug)]
struct ImportantExcerpt<'a> {
    part: &'a str,
}

// Lifetime Annotations in Method Definitions
// first rule applies here
impl<'a> ImportantExcerpt<'a> {
    fn level(&self) -> i32 {
        5
    }
}
// third rule applies here
impl<'a> ImportantExcerpt<'a> {
    fn announce_and_return_part(&self, announcement: &str) -> &str {
        println!("Attention please: {announcement}");
        self.part
    }
}

fn struct_lifetimes() {
    let novel = String::from("Call me Ishmael. Some years ago...");
    let first_sentence = novel
        .split('.')
        .next()
        .expect("Could not find a '.'");
    let i = ImportantExcerpt {
        part: first_sentence,
    };
    println!("important excerpt: {:?}", i);
}

fn function_lifetimes() {
    let string1 = String::from("abcd");
    let string2 = "xyz";

    let result = longest(string1.as_str(), string2);
    println!("The longest string is {result}");

    let string1 = String::from("long string is long");
    {
        let string2 = String::from("xyz");
        let result = longest(string1.as_str(), string2.as_str());
        println!("The longest string is {result}");
    }

/*    let string1 = String::from("long string is long");
    let result;
    {
        let string2 = String::from("xyz");
        // next line will not compile, because string2 had shorter lifetimes
        result = longest(string1.as_str(), string2.as_str());
    }
    println!("The longest string is {result}");
*/
}

fn dangling_refs() {
    println!("Preventing Dangling References with Lifetimes");
    // the code below won't compile
/*    let r;
    {
        let x = 5;
        r = &x; // compilation fails here with error: borrowed value does not live long enough
    }
    println!("r: {}", r);*/
}

fn no_dangling_ref() {
    let x = 5;
    let r = &x;
    println!("r: {}", r);
}

// next function won't compile
// error: this function's return type contains a borrowed value, but the signature does not say whether it is borrowed from `x` or `y`
/*fn longest(x: &str, y: &str) -> &str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
*/

// problem in previous function can be solved with 'a lifetimes annotation
fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}

// no lifetimes annotation on y and code compiles
fn longest2<'a>(x: &'a str, y: &str) -> &'a str {
    x
}

// next function won't compile because of dangling reference
/*fn longest3<'a>(x: &str, y: &str) -> &'a str {
    let result = String::from("really long string");
    result.as_str()
}
*/
