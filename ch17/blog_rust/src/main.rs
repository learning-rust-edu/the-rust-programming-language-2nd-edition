use blog_rust::Post;

fn main() {
    let mut post = Post::new();

    post.add_text("I ate a salad for lunch today");
    // next line won't compile
    // assert_eq!("", post.content());

    let post = post.request_review();
    // next line won't compile
    // assert_eq!("", post.content());

    let post = post.approve();
    assert_eq!("I ate a salad for lunch today", post.content());
}
