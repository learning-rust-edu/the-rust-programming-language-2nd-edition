use encapsulation::{AveragedCollection, Button, Draw, Screen};

fn main() {
    let mut average_list = AveragedCollection::new();
    average_list.add(1);
    average_list.add(2);
    average_list.add(3);
    average_list.add(4);
    average_list.add(5);
    println!("average = {}", average_list.average());
    average_list.remove();
    println!("average = {}", average_list.average());
    println!();

    polymorphism();
}

fn polymorphism() {
    let screen = Screen {
        components: vec![
            Box::new(SelectBox {
                width: 75,
                height: 10,
                options: vec![
                    String::from("Yes"),
                    String::from("Maybe"),
                    String::from("No"),
                ],
            }),
            Box::new(Button {
                width: 50,
                height: 10,
                label: String::from("OK"),
            }),
        ],
    };

    screen.run();
}

struct SelectBox {
    width: u32,
    height: u32,
    options: Vec<String>,
}

impl Draw for SelectBox {
    fn draw(&self) {
        // code to actually draw a select box
        println!("Drawing select box with width {}, height {} and options {:?}",
               self.width, self.height, self.options);
    }
}
