use std::io;

pub fn variables(with_input: bool) {
    // mutable vs immutable variables
    let mut x = 5;
    println!("The value of x is: {}", x);
    x = 6;
    println!("The value of x is: {}", x);

    let y = 7;
    println!("The value of y is: {}", y);
    // next line won't compile, because variable y is immutable
    // y = 8;
    println!();

    // constants
    const THREE_HOURS_IN_SECONDS: u32 = 60 * 60 * 3;
    println!("Three hours in seconds = {}", THREE_HOURS_IN_SECONDS);
    // constants are always immutable and cannot be made mutable or to be redeclared
    println!();

    // Shadowing
    let z = 5;
    let z = z + 1;
    {
        let z = z * 2;
        println!("The value of z in the inner scope is: {z}");
    }
    println!("The value of z is: {z}");

    // shadowing can change variable type
    let spaces = "   ";
    let spaces = spaces.len();
    println!("Spaces: {spaces}");
    println!();

    // Data Types
    let guess: u32 = "42".parse().expect("Not a number!");
    println!("Answer is {guess}");

    // Scalar types
    // Integer types
    let a = 123;
    println!("{}", a);

    // floating point
    let b = 2.0; // f64
    let c: f32 = 3.0; // f32
    println!("{} and {}", b, c);
    println!();

    // numeric operation
    // addition
    let sum = 5 + 10;
    println!("sum {}", sum);

    // subtraction
    let difference = 95.5 - 4.3;
    println!("difference {}", difference);

    // multiplication
    let product = 4 * 30;
    println!("product {}", product);

    // division
    let quotient = 56.7 / 32.2;
    let truncated = -5 / 3; // Results in -1
    println!("quotient {}", quotient);
    println!("truncated {}", truncated);

    // remainder
    let remainder = 43 % 5;
    println!("remainder {}", remainder);
    println!();

    // boolean type
    let t = true;
    let f: bool = false; // with explicit type annotation
    println!("{} and {}", t, f);
    println!();

    // Character type
    let c = 'z';
    let z: char = 'ℤ'; // with explicit type annotation
    let heart_eyed_cat = '😻';
    println!("{}, {}, {}", c, z, heart_eyed_cat);
    println!();

    // Compound Types
    // Tuple
    let tup: (i32, f64, u8) = (500, 6.4, 1);
    println!("{:?}", tup);

    let tup = (500, 6.4, 1);
    let (_x, y, _z) = tup;
    println!("The value of y is: {y}");

    let x: (i32, f64, u8) = (500, 6.4, 1);

    let five_hundred = x.0;
    let six_point_four = x.1;
    let one = x.2;
    println!("{}, {}, {}", five_hundred, six_point_four, one);
    println!();

    // Array
    let a = [1, 2, 3, 4, 5];
    println!("{:?}", a);

    let months = ["January", "February", "March", "April", "May", "June", "July",
        "August", "September", "October", "November", "December"];
    println!("{:?}", months);

    let a: [i32; 5] = [1, 2, 3, 4, 5]; // define array's type and size
    println!("{:?}", a);

    // initialize an array to contain the same value for each element by specifying the initial
    // value, followed by a semicolon, and then the length of the array
    let a = [3; 5];
    println!("{:?}", a);
    println!();

    let a = [1, 2, 3, 4, 5];
    let first = a[0];
    let second = a[1];
    println!("{}, {}", first, second);
    println!();

    if with_input {
        get_array_index();
        println!();
    }
}

fn get_array_index() {
    let a = [1, 2, 3, 4, 5];

    println!("Please enter an array index.");

    let mut index = String::new();

    io::stdin()
        .read_line(&mut index)
        .expect("Failed to read line");

    let index: usize = index
        .trim()
        .parse()
        .expect("Index entered was not a number");

    let element = a[index];

    println!("The value of the element at index {index} is: {element}");
}
