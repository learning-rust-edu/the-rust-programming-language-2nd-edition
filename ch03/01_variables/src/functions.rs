pub fn functions() {
    another_function();
    // parameters
    another_function2(5);
    print_labeled_measurement(5, 'h');
    println!();

    // statements and expressions
    statements_etc();
    println!();

    // functions with return values
    functions_with_return_values();
    println!();
}

fn another_function() {
    println!("Another function.");
}

fn another_function2(x: i32) {
    println!("The value of x is: {x}");
}

fn print_labeled_measurement(value: i32, unit_label: char) {
    println!("The measurement is: {value}{unit_label}");
}

fn statements_etc() {
    let y = 6; // this is statement
    println!("{}", y);
    // Statements do not return values. Therefore, can’t assign a let statement to another variable
    // next line will cause compile error
//    let x = (let y = 6);

    // expressions
    let y = {
        let x = 3;
        x + 1
    };
    println!("The value of y is: {y}");
}

fn functions_with_return_values() {
    let x = five();
    println!("The value of x is: {x}");

    let x = plus_one(5);
    println!("The value of x is: {x}");
}

fn five() -> i32 {
    5
}

fn plus_one(x: i32) -> i32 {
    x + 1
}

