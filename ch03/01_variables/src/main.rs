mod variables;
mod functions;
mod control_flow;

fn main() {
    variables::variables(false);
    functions::functions();
    control_flow::control_flow();
}
