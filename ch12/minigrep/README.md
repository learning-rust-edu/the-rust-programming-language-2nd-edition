# Command line program: minigrep

## Run program

To run the program execute following command:

```shell
cargo run -- searchstring example-filename.txt
```

In this command `searchstring` is a string, which will be searched in the file.
`example-filename.txt` is the name of the file, which will be searched.
