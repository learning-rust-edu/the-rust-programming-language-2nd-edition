use std::collections::HashMap;

fn main() {
    // Storing Keys with Associated Values in Hash Maps

    // Creating a New Hash Map
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);
    println!("{:?}", scores);

    // Accessing Values in a Hash Map
    let team_name = String::from("Blue");
    let score = scores.get(&team_name).copied().unwrap_or(0);
    println!("Score for team '{}' is {}", team_name, score);
    println!();

    // using for loop
    for (key, value) in &scores {
        println!("Score for team '{}' is {}", key, value);
    }
    println!();

    // Hash Maps and Ownership
    let field_name = String::from("Favorite color");
    let field_value = String::from("Blue");

    let mut map = HashMap::new();
    map.insert(field_name, field_value);
    // field_name and field_value are invalid at this point, try using them and see what compiler error you get!
    // println!("field_name: {}", field_name);
    // println!("field_value: {}", field_value);
    println!("map: {:?}", map);
    println!();

    // Updating a Hash Map
    // Overwriting a Value
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Blue"), 25);
    println!("{:?}", scores);

    // Adding a Key and Value Only If a Key Isn’t Present
    scores.entry(String::from("Yellow")).or_insert(50); // will add new entry
    scores.entry(String::from("Blue")).or_insert(50); // will not update existing entry
    println!("{:?}", scores);
    println!();

    // Updating a Value Based on the Old Value
    let text = "hello world wonderful world";
    let mut map = HashMap::new();
    for word in text.split_whitespace() {
        let count = map.entry(word).or_insert(0);
        *count += 1;
    }
    println!("{:?}", map);
}
