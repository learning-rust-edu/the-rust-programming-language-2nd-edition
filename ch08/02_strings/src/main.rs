fn main() {
    // Strings in Rust
    // Creating a New String
    create_string();
    println!();
    // UTF-8 supported out of the box
    utf_8();
    println!();
    // Updating a String
    update_string();
    println!();
}

fn create_string() {
    let s = String::new();
    println!("{}", s);

    let data = "initial contents";
    println!("created from literal is of type &str '{}'", data);

    let s = data.to_string();
    println!("converted from &str into String with .to_string() method '{}'", s);

// The method also works on a literal directly:
    let s = "another initial contents".to_string();
    println!("converted from string literal into String '{}'", s);

    let s = String::from("initial contents again");
    println!("created with String::from '{}'", s);
}

fn utf_8() {
    let greetings = vec![
        String::from("السلام عليكم"),
        String::from("Dobrý den"),
        String::from("Hello"),
        String::from("שָׁלוֹם"),
        String::from("नमस्ते"),
        String::from("こんにちは"),
        String::from("안녕하세요"),
        String::from("你好"),
        String::from("Olá"),
        String::from("Здравствуйте"),
        String::from("Hola"),
    ];
    println!("greetings: {:#?}", greetings);
}

fn update_string() {
    let mut s = String::from("foo");
    s.push_str("bar");
    println!("{}", s);

    let mut s1 = String::from("foo");
    let s2 = "bar";
    s1.push_str(s2);
    println!("s2 is {s2}");
    println!("s1 is {s1}");

    let mut s = String::from("lo");
    s.push('l');
    println!("{}", s);
    println!();

    // Concatenation with the + Operator or the format! Macro
    concatenate_strings();
    println!();

    // Indexing into Strings
    indexing_strings();
    println!();

    // Slicing strings
    slicing_strings();
    println!();

    // Methods for Iterating Over Strings
    iterate_string();
}

fn concatenate_strings() {
    let s1 = String::from("Hello, ");
    let s2 = String::from("world!");
    let s3 = s1 + &s2; // note s1 has been moved here and can no longer be used
    println!("{}", s3);

    let s1 = String::from("tic");
    let s2 = String::from("tac");
    let s3 = String::from("toe");
    let s = s1 + "-" + &s2 + "-" + &s3;
    println!("{}", s);

    // same as above but with format! macro
    let s1 = String::from("tic");
    let s2 = String::from("tac");
    let s3 = String::from("toe");
    let s = format!("{s1}-{s2}-{s3}");
    println!("{}", s);
}

fn indexing_strings() {
    let s1 = String::from("hello");
    // next line will not compile
//    let h = s1[0];

    // Internal Representation
    let hello = String::from("Hola");
    println!("'{}' has length {}", hello, hello.len());

    let hello = String::from("Здравствуйте");
    println!("'{}' has length {}", hello, hello.len());

    let hello = String::from("नमस्ते");
    println!("'{}' has length {}", hello, hello.len());
}

fn slicing_strings() {
    let hello = "Здравствуйте";
    let s = &hello[0..4];
    println!("{}", s);
}

fn iterate_string() {
    // iterating by chars
    for c in "Зд".chars() {
        println!("{c}");
    }

    // iterating by bytes
    for b in "Зд".bytes() {
        println!("{b}");
    }
}
