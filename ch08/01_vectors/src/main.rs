fn main() {
    // Storing Lists of Values with Vectors
    // Creating a new vector
    let v: Vec<i32> = Vec::new();
    println!("{:?}", v);

    // or using macro
    let v = vec![1, 2, 3];
    println!("{:?}", v);

    // Updating a Vector
    let mut v = Vec::new();
    v.push(5);
    v.push(6);
    v.push(7);
    v.push(8);
    println!("{:?}", v);
    println!();

    // Reading Elements of Vectors
    let v = vec![1, 2, 3, 4, 5];
    let third: &i32 = &v[2];
    println!("The third element is {third}");

    // or using .get() method
    let third: Option<&i32> = v.get(2);
    match third  {
        Some(third) => println!("The third element is {third}"),
        None => println!("There is no third element."),
    }

    let v = vec![1, 2, 3, 4, 5];

    // next line will crash with 'index out of bounds' error
//    let does_not_exist = &v[100];
    let does_not_exist = v.get(100);
    println!("Not existing element {:?}", does_not_exist);
    println!();

    // Iterating Over the Values in a Vector
    let v = vec![100, 32, 57];
    for i in &v {
        println!("{i}");
    }
    println!();

    let mut v = vec![100, 32, 57];
    println!("{:?}", v);
    for i in &mut v {
        *i += 50;
    }
    println!("{:?}", v);
    println!();

    // Using an Enum to Store Multiple Types
    let row = vec![
        SpreadsheetCell::Int(3),
        SpreadsheetCell::Text(String::from("blue")),
        SpreadsheetCell::Float(10.12),
    ];
    println!("{:?}", row);
    println!();

    // Dropping a Vector Drops Its Elements
    drop_vec();
    println!();
}

#[derive(Debug)]
enum SpreadsheetCell {
    Int(i32),
    Float(f64),
    Text(String),
}

fn drop_vec() {
    {
        let v = vec![1, 2, 3, 4];

        // do stuff with v
        println!("{:?}", v);
    } // <- v goes out of scope and is freed here
    // next line will not compile, v is not in scope
//    println!("{:?}", v);
}
