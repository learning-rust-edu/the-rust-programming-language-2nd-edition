use std::cell::RefCell;
use std::ops::Deref;
use std::rc::Rc;
use crate::List::{Cons, Nil};
use crate::ListRc::{ConsRc, NilRc};
use crate::ListRf::{ConsRf, NilRf};

#[derive(Debug)]
enum List {
    Cons(i32, Box<List>),
    Nil,
}

#[derive(Debug)]
struct MyBox<T>(T);

impl<T> MyBox<T> {
    fn new(x: T) -> MyBox<T> {
        MyBox(x)
    }
}

// Implementing the Deref Trait
impl <T> Deref for MyBox<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

struct CustomSmartPointer {
    data: String,
}

impl Drop for CustomSmartPointer {
    fn drop(&mut self) {
        println!(
            "Dropping CustomSmartPointer with data `{}`!",
            self.data
        );
    }
}

fn main() {
    let b = Box::new(5);
    println!("b = {b}");

    // Enabling Recursive Types with Boxes
    let list = Cons(
        1,
        Box::new(Cons(
            2,
            Box::new(Cons(
                3,
                Box::new(Nil))))));
    println!("list: {:?}", list);

    // Following the Pointer to the Value
    let x = 5;
    let y = &x; // create a reference to x

    assert_eq!(5, x);
    assert_eq!(5, *y); // dereference y to get value it is pointing to

    // Using Box<T> Like a Reference
    // rewrite the code above with Box<T>
    let x = 5;
    let y = Box::new(x);

    assert_eq!(5, x);
    assert_eq!(5, *y); // dereference y to get value it is pointing to

    // Defining Our Own Smart Pointer
    let x = 5;
    let y = MyBox::new(x);

    assert_eq!(5, x);
    assert_eq!(5, *y); // this requires implementation of Deref trait
    println!("y = {:?}", y);
    println!();

    // Implicit Deref Coercions with Functions and Methods
    let m = MyBox::new(String::from("Rust"));
    hello(&m);
    println!();

    // Running Code on Cleanup with the Drop Trait
    println!("about to start clean up experiment");
    code_cleanup();
    println!("clean up experiment ended");
    println!();

    // Rc<T>, the Reference Counted Smart Pointer
    use_rc();
    println!();

    // Allowing Multiple Owners of Mutable Data with Rc<T> and RefCell<T>
    rc_and_ref_cell();
    println!();
}

#[derive(Debug)]
enum ListRf {
    ConsRf(Rc<RefCell<i32>>, Rc<ListRf>),
    NilRf,
}

fn rc_and_ref_cell() {
    let value = Rc::new(RefCell::new(5));

    let a = Rc::new(ConsRf(Rc::clone(&value), Rc::new(NilRf)));
    let b = ConsRf(Rc::new(RefCell::new(3)), Rc::clone(&a));
    let c = ConsRf(Rc::new(RefCell::new(4)), Rc::clone(&a));

    *value.borrow_mut() += 10;

    println!("a after = {:?}", a);
    println!("b after = {:?}", b);
    println!("c after = {:?}", c);
}

#[derive(Debug)]
enum ListRc {
    ConsRc(i32, Rc<ListRc>),
    NilRc,
}

fn use_rc() {
    let a = Rc::new(ConsRc(5, Rc::new(ConsRc(10, Rc::new(NilRc)))));
    println!(
        "count after creating a = {}",
        Rc::strong_count(&a)
    );
    let b = ConsRc(3, Rc::clone(&a));
    println!(
        "count after creating b = {}",
        Rc::strong_count(&a)
    );
    {
        let c = ConsRc(4, Rc::clone(&a));
        println!(
            "count after creating c = {}",
            Rc::strong_count(&a)
        );
    }
    println!(
        "count after c goes out of scope = {}",
        Rc::strong_count(&a)
    );
}

fn code_cleanup() {
    let a = CustomSmartPointer {
        data: String::from("my stuff a"),
    };
    let b = CustomSmartPointer {
        data: String::from("my stuff b"),
    };
    let c = CustomSmartPointer {
        data: String::from("my stuff c"),
    };
    let d = CustomSmartPointer {
        data: String::from("other stuff"),
    };
    drop(c);
    println!("CustomSmartPointers created.");
}

fn hello(name: &str) {
    println!("Hello, {name}!");
}
